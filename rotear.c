/*
 * Programa: rotear.c
 * Descrição: Esse programa tem o objetivo de calcular a distância mais curta entre dois vértices, e oferece 3 maneiras
 * de fazer isso: Dijkstra convencional, Dijkstra Alternante e Dijkstra com Heuristica (A*).
 * Autores: Aimée S. Calepso e Rogério J. Sandim
 * Data: Abril/2012
 */

//bibliotecas
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<stdbool.h>
#include<limits.h>
#define INTMAX 10000000.000
#define RAIO 6356.75
#define PI 3.141592653589793

//Estrutura para armazenar as informações dos vizinhos e da heap
typedef struct
{
  int num;
  double peso;
  
}vizinho;
//Estrutura para armazenar as informações de cada vértice
typedef struct
{
  int num, nViz;
  double lat;
  double longi;
  vizinho* lista;  
}vertice;

typedef vertice * GRAFO;

/*Protótipos das funções */
void leGrafo(GRAFO*, FILE*, int*);
void leGrafoI(GRAFO *, GRAFO *, FILE*, int *);
void inverteGrafo(GRAFO, GRAFO, int);
void dijkstra(GRAFO, int, int, int);
void dijkstraAlter(GRAFO, GRAFO, int, int , int);
void aEstrela(GRAFO, int, int , int);
void imprimeCaminho(int, int, int*, double *);
double calculaDist(vertice, vertice);

/*Funções da heap */
void troca(vizinho *, vizinho *, int);
int pai(int);
int esquerdo(int);
int direito(int);
void desce(int, vizinho *,int);
void sobe(int, vizinho *, int);
void constroi_min_heap(int, vizinho *);
double consulta_minimo(vizinho *);
int extrai_minimo(int *, vizinho *, double *);
void diminui_prioridade(int, vizinho *, int, double);
int procuraHeap(int, int, vizinho*);
void insere_lista(int *, vizinho*, int, double);

int main(int argc, char *argv[])
{
 
  //declaração das variáveis
  int orig, dest, alg, v;
  GRAFO G, I;
  FILE *arq;
  v=1;
  alg = atoi(argv[2]);
  orig = atoi(argv[3]);
  dest = atoi(argv[4]);
  
  arq = fopen(argv[1], "r");
   
  //Algoritmo de Dijkstra simples
  if(alg == 1)
  {
      leGrafo(&G, arq, &v);
      fclose(arq);
      dijkstra(G, orig, v, dest);
    
  }else if(alg == 2) //Algoritmo de Dijkstra Alternante
  {
    leGrafoI(&G, &I, arq, &v);
    fclose(arq); //  dijkstraAlter(G, orig, v, dest);
    inverteGrafo(G,I, v);
    dijkstraAlter(G, I, orig, v, dest);  
    free(I);
  }else if(alg == 3) //Algoritmo de Dijkstra com heuristica 
  {
    leGrafo(&G, arq, &v);
    fclose(arq);
    aEstrela(G, orig, v, dest);
  }
  
  free(G);
  
  return 0;
}

/*Essa função recebe um vetor de vértices e um ponteiro para leitura de arquivo, 
 *lê os dados referentes ao grafo, armazenando-os no vetor e fazendo as alocações
 * de memória necessárias*/
void leGrafo(GRAFO *G, FILE* arq, int *n)
{
  GRAFO aux;
  int v, e, grauMax, i, j, num;
  char c;
  
  fscanf(arq, "%c %d %d %d", &c, &v, &e, &grauMax);
  
  //alocação da matriz para armazenar o grafo
  aux = (GRAFO)malloc(sizeof(vertice)*v);
  for(i = 0; i<v; i++)
  {
    fscanf(arq,"\n%c %d %lf %lf %d", &c, &aux[i].num, &aux[i].lat, &aux[i].longi, &aux[i].nViz);
    num = aux[i].nViz;
    aux[i].lista = (vizinho*)malloc(sizeof(vizinho)*num); 
   
    for(j = 0; j <aux[i].nViz; j++)
    {
      fscanf(arq, "%d", &aux[i].lista[j].num);
      fscanf(arq, "%lf", &aux[i].lista[j].peso);
    }
   
  }

  *G = aux;
   *n = v;
}


/*Essa função recebe um vetor de vértices e um ponteiro para leitura de arquivo, 
 *lê os dados referentes ao grafo, armazenando-os no vetor e fazendo as alocações
 * de memória necessárias também para o grafo inverso do grafo original*/
void leGrafoI(GRAFO *G, GRAFO *I, FILE* arq, int *n)
{
  GRAFO aux, aux2;
  int v, e, grauMax, i, j, num;
  char c;
  
  
  fscanf(arq, "%c %d %d %d", &c, &v, &e, &grauMax);
  //alocação da matriz para armazenar o grafo
  aux = (GRAFO)malloc(sizeof(vertice)*v);
  aux2 = (GRAFO)malloc(sizeof(vertice)*v);
  for(i = 0; i<v; i++)
  {
    fscanf(arq,"\n%c %d %lf %lf %d", &c, &aux[i].num, &aux[i].lat, &aux[i].longi, &aux[i].nViz);
    aux2[i].num = aux[i].num;
    aux2[i].lat = aux[i].lat;
    aux2[i].longi = aux[i].longi;
    aux2[i].nViz = 0;
    num = aux[i].nViz;
  
    aux[i].lista = (vizinho*)malloc(sizeof(vizinho)*num); 
   
    for(j = 0; j <aux[i].nViz; j++)
    {
      fscanf(arq, "%d", &aux[i].lista[j].num);
      fscanf(arq, "%lf", &aux[i].lista[j].peso);
    }
  }
  *G = aux;
  *I = aux2;
   *n = v;
}

/*Recebe dois grafos G e I e faz com que I 
seja o grafo G com suas arestas no sentido contrário */
void inverteGrafo(GRAFO G, GRAFO I, int n)
{
  int i, j, k;
  
  //para cada um dos vertices de G
  for(i = 0; i<n; i++)
  {
    //para cada um dos vizinhos de G
    for(j=0; j<G[i].nViz; j++)
    {
      //o vizinho k no grafo I terá como vizinho o vértice i com o peso de (i, k)
      k = G[i].lista[j].num-1;
      
      
      //verifica se a lista de vizinhos é vazia
      if(I[k].nViz == 0)
      {
	I[k].nViz++;
	I[k].lista = (vizinho*)malloc(sizeof(vizinho)*I[k].nViz); 
	I[k].lista[I[k].nViz-1].num = i+1;
	I[k].lista[I[k].nViz-1].peso = G[i].lista[j].peso;
	
      }
      //se a lista já tiver algum vizinho, cria o espaço para o novo vizinho e seu peso
      else 
      {
	I[k].nViz++;
	I[k].lista = (vizinho*)realloc(I[k].lista, sizeof(vizinho)*I[k].nViz); 
	I[k].lista[I[k].nViz-1].num = i+1;
	I[k].lista[I[k].nViz-1].peso = G[i].lista[j].peso;	
      }     
    }
  }
}



/* Essa função recebe um grafo G, o numero de vértices do grafo e os vértices de origem e destino, calcula a menor distancia 
 * entre esses vértices utilizando o algoritmo de Dijkstra e exibe o caminho percorrido da origem até o destino*/
void dijkstra(GRAFO G, int inicio, int n, int final)
{
  vizinho Q[n]; //vetor da heap
  int i, u, indiceViz, a, achou;
  double distancia[n], pesoViz, pesoAtual;
  int anterior[n];
  int visitado[n];
  
  inicio = inicio-1; //decrementa o vértice de inicio para acertar os índices
  
  //inicia os vetores de vértices visitados e dos vértices anteriores(que formam o caminho)
  for(i = 0; i<n; i++)
  {
    visitado[i] = 0;
    anterior[i] = -1; 
    distancia[i] = INTMAX; 
    
  }
  //marca a distancia do vértice inicial como zero
  distancia[inicio] = 0;
  
  //Cria a heap de tamanho 1 com o primeiro vértice 
  Q[0].num = inicio+1;
  Q[0].peso = 0;
  n = 1;
  constroi_min_heap(n, Q);     
  //zera a distancia do primeiro vértice 
  distancia[inicio] = 0;
  //zera o contador de vértices consultados
  a = 0;
  
 //enquanto o vértice final não for escolhido ou nenhum outro vértice puder ser alcançado
  while(n > 0 && visitado[final-1] == 0)
  {
    //retira o menor elemento da heap
    u = extrai_minimo(&n,Q,&pesoAtual);
    //marca o elemento como visitado
    visitado[u] = 1;
    
   //para cada um dos vizinhos do vértice atual    
    for(i = 0; i<G[u].nViz; i++)
    {
       
      pesoViz = G[u].lista[i].peso; //guarda o peso do vizinho atual 
      indiceViz = G[u].lista[i].num - 1; //guarda o indice do vizinho atual
     
     //verifica se o caminho encontrado é melhor que o caminho que já existia antes
     if(distancia[indiceViz] > pesoAtual+pesoViz && visitado[indiceViz] == 0) 
     {	
	anterior[indiceViz] = u+1; //marca o vértice u como anterior de seu vizinho 
	distancia[indiceViz] = pesoAtual+pesoViz; //atualiza a distancia
	achou = procuraHeap(indiceViz+1, n, Q); //verifica se o vértice já existe na heap
	if(achou > -1)
	{
	  //atualiza a distancia se ele já estiver na heap
	  diminui_prioridade(n, Q,indiceViz+1, distancia[indiceViz]); //atualiza o caminho do vértice na heap (muda prioridade)
	}else
	{
	  //caso contrário, adiciona o vértice na heap
	  insere_lista(&n, Q, indiceViz+1, distancia[indiceViz]);
	}  
     }
    }
    a++;
   } //fim do loop, todos os vértices foram visitados ou não é possível chegar em mais nenhum vértice
    
    //verifica se o vértice de destino foi visitado
    if(visitado[final-1])
    {
      //imprime a distancia total do camiho mais curto
      printf("1 %.3lf\n", distancia[final-1]);
      imprimeCaminho(inicio, (final-1), anterior, distancia); //imprime o menor caminho da origem até o destino
    }
    else
    {
      printf("0 0.000\n%d 0.000 %d 0.000 ", inicio+1, final);
    }
    printf("\n%d\n", a);
   
}



/* Essa função recebe um grafo G e um grafo I(que é o inverso de G), o numero de vértices desses grafos e os vértices de origem e destino, calcula a menor distancia 
 * entre esses vértices utilizando o algoritmo de Dijkstra, com uma alteração que melhora o tempo e diminui o numero total de vértices consultados,
 * e exibe o caminho percorrido da origem até o destino*/

void dijkstraAlter(GRAFO G, GRAFO I, int inicio, int n, int final)
{
  vizinho Q[n], P[n]; //vetores da heap
  int i, u, p, indiceViz, m, achou, meio, a;
  double distanciaOrig[n], distanciaDest[n], pesoViz, pesoAtualOrig, pesoAtualDest, pesoMin;
  int anteriorOrig[n], anteriorDest[n], visitadoOrig[n], visitadoDest[n];
  
  m = n; //m servirá de indice para a heap do destino
  inicio = inicio-1; //decrementa o vértice de inicio para acertar os índices
  final = final-1; //decrementa o vértice de destino para acertar os índices
  
  //inicia os vetores de vértices visitados e dos vértices anteriores(que formam o caminho)
  for(i = 0; i<n; i++)
  {
      visitadoOrig[i] = 0;
      visitadoDest[i] = 0;
      anteriorOrig[i] = -1;
      anteriorDest[i] = -1;
      distanciaOrig[i] = INTMAX;
      distanciaDest[i] = INTMAX;
    
  }
  //marca a distancia do vértice inicial e final como zero
  distanciaOrig[inicio] = 0;
  distanciaDest[final] = 0;
  
  //inicializa a heap e o vetor de distâncias  
  u = inicio;
  p = final;

  Q[0].num = G[u].num;
  Q[0].peso = 0;  
  P[0].num = I[p].num;
  P[0].peso = 0;    
  
  //Cria a heap de tamanho 1 com o primeiro vértice 
  n = 1;
  constroi_min_heap(n, Q);  
  m = 1;
  constroi_min_heap(m, P);  
  //zera a distancia do primeiro vértice 
  
  //zera o contador de vértices consultados
  a = 0;
  meio = 0;
  
 //enquanto houverem vértices não visitados ou nenhum outro vértice pode ser alcançado
  while(n > 0 && m >0)
  {    
      if(consulta_minimo(Q) <= consulta_minimo(P))
      {
	
	   //retira o menor elemento da heap
	  u = extrai_minimo(&n,Q,&pesoAtualOrig);
	  //marca o elemento como visitado
	  visitadoOrig[u] = 1;
	 
	  if(visitadoDest[u] == 1)
	  {
	    meio = u+1;
	    a++;
	    break;
	    
	  }
	//para cada um dos vizinhos do vértice atual, da origem para o destino  
	  for(i = 0; i<G[u].nViz; i++)
	  {
	      //atribuições apenas para facilitar a leitura do código
	      pesoViz = G[u].lista[i].peso; //guarda o peso do vizinho atual 
	      indiceViz = G[u].lista[i].num - 1; //guarda o indice do vizinho atual
	      
	      //verifica se o caminho encontrado é melhor que o caminho que já existia antes
	      if(distanciaOrig[indiceViz] > distanciaOrig[u]+pesoViz && visitadoOrig[indiceViz] == 0) 
	      { 
		  anteriorOrig[indiceViz] = u+1; //marca o vértice u como anterior de seu vizinho 
		  distanciaOrig[indiceViz] = distanciaOrig[u]+pesoViz; //atualiza a distancia
		  achou = procuraHeap(indiceViz+1, n, Q); //verifica se o vértice já existe na heap
		  if(achou > -1) //atualiza a distancia se ele já estiver na heap
		  {
		    diminui_prioridade(n, Q,indiceViz+1, distanciaOrig[indiceViz]); //atualiza o caminho do vértice na heap (muda prioridade)
		  }else //caso contrário, adiciona o vértice na heap
		  {
		    insere_lista(&n, Q, indiceViz+1, distanciaOrig[indiceViz]);
		  }  
	    }
	    }
	}else
	{
	  //remove o menor elemento da heap
	  p = extrai_minimo(&m,P,&pesoAtualDest);
	  //marca o elemento como visitado
	  visitadoDest[p] = 1;
	 
	  if(visitadoOrig[p] == 1)
	  {
	    meio = p+1;
	    a++;
	    break;
	  }
	  
	  //para cada um dos vizinhos do vértice atual, do destino para a origem 
	  for(i = 0; i<I[p].nViz; i++)
	  {
	    //atribuições apenas para facilitar a leitura do código
	    pesoViz = I[p].lista[i].peso; //guarda o peso do vizinho atual 
	    indiceViz = I[p].lista[i].num - 1; //guarda o indice do vizinho atual
	    //verifica se o caminho encontrado é melhor que o caminho que já existia antes
	    if(distanciaDest[indiceViz] > distanciaDest[p]+pesoViz) 
	    {
	      distanciaDest[indiceViz] = distanciaDest[p]+pesoViz;
	      anteriorDest[indiceViz] = p+1; //marca o vértice u como anterior de seu vizinho 
	      achou = procuraHeap(indiceViz+1, m, P); //verifica se o vértice já existe na heap
	      if(achou > -1)
	      {
		//atualiza a distancia se ele já estiver na heap
		diminui_prioridade(m, P,indiceViz+1, distanciaDest[indiceViz]); //atualiza o caminho do vértice na heap (muda prioridade)
	      }else
	      {
		//caso contrário, adiciona o vértice na heap
		insere_lista(&m, P, indiceViz+1, distanciaDest[indiceViz]);
	      } 
	    }
	  }
	}
	a++;
	
	
    } //fim do loop, todos os vértices foram visitados ou não é possível chegar em mais nenhum vértice
  
    if(meio!=0)
    {
      pesoMin = distanciaOrig[meio-1] + distanciaDest[meio-1];
      for(i = 0; i<n; i++)
      {
      
	if(Q[i].peso + distanciaDest[Q[i].num-1] < pesoMin)
	{
	  meio = Q[i].num;
	  pesoMin = Q[i].peso + distanciaDest[Q[i].num-1];
	}	
      }
      for(i = 0; i<m; i++)
      {
      
	if(P[i].peso + distanciaOrig[P[i].num-1] < pesoMin) 
	{
	  meio = P[i].num;
	  pesoMin = P[i].peso + distanciaOrig[P[i].num-1];
	}
      }
    }
  
 
//se o destino foi encontrado, imprime   o caminho
  if(meio != 0)
  {
 
    printf("1 %.3lf\n",pesoMin); 
   
   imprimeCaminho(inicio, meio-1, anteriorOrig, distanciaOrig); //imprime o menor caminho da origem até o destino
   while(meio!=final+1)
   {
    printf("%d %.3lf ", meio, distanciaDest[meio-1]);
    meio = anteriorDest[meio-1];
     
   }
    printf("%d %.3lf ", meio, distanciaDest[meio-1]);
     
  }
  else
  {
    printf("0 0.000\n%d 0.000 %d 0.000 ", inicio+1, final+1);
  }
  printf("\n%d\n", a);
}



/* Essa função recebe um grafo G, o numero de vértices do grafo e os vértices de origem e destino, calcula a menor distancia 
 * entre esses vértices utilizando o algoritmo de Dijkstra com uma modificação que utiliza uma heuristica para melhorar o tempo
 * de execução do algoritmo e reduz o numero de vértices visitados, e exibe o caminho percorrido da origem até o destino*/
void aEstrela(GRAFO G, int inicio, int n, int final)
{
  vizinho Q[n]; //vetor da heap
  int i, u, indiceViz, a, achou;
  double distancia[n], pesoViz, pesoAtual, limitante[n], lim;
  int anterior[n];
  int visitado[n];
  
  inicio = inicio-1; //decrementa o vértice de inicio para acertar os índices
  
  //inicia os vetores de vértices visitados e dos vértices anteriores(que formam o caminho)
  for(i = 0; i<n; i++)
  {
    visitado[i] = 0;
    anterior[i] = -1; 
    distancia[i] = INTMAX;
    limitante[i] = INTMAX;
    
  }
  //marca a distancia do vértice inicial como zero
  distancia[inicio] = 0;
  
  //Cria a heap de tamanho 1 com o primeiro vértice 
  Q[0].num = inicio+1;
  Q[0].peso = 0;
  n = 1;
  constroi_min_heap(n, Q);  
   
  //zera a distancia do primeiro vértice 
  distancia[inicio] = 0;
  //zera o contador de vértices consultados
  a = 0;
  
 //enquanto o vértice final não for escolhido ou nenhum outro vértice puder ser alcançado
  while(n > 0 && visitado[final-1] == 0)
  {
    //retira o menor elemento da heap
    u = extrai_minimo(&n,Q,&pesoAtual);
    //marca o elemento como visitado
    visitado[u] = 1;

   //para cada um dos vizinhos do vértice atual    
    for(i = 0; i<G[u].nViz; i++)
    { 
      //atribuições apenas para facilitar a leitura do código
      pesoViz = G[u].lista[i].peso;
      indiceViz = G[u].lista[i].num - 1; //guarda o indice do vizinho atual
      lim = calculaDist(G[indiceViz], G[final-1]);
      
      if(visitado[indiceViz] == 0)
      {
	pesoViz = G[u].lista[i].peso; //guarda o peso do vizinho atual 
	
	//verifica se o caminho encontrado é melhor que o caminho que já existia antes
	if(limitante[indiceViz] > distancia[u] + pesoViz + lim) 
	{
	  distancia[indiceViz] = distancia[u]+pesoViz; //atualiza a distancia
	  limitante[indiceViz] = lim + distancia[indiceViz]; //atualiza o limitante do vizinho
	  anterior[indiceViz] = u+1; //marca o vértice u como anterior de seu vizinho 

	  achou = procuraHeap(indiceViz+1, n, Q); //verifica se o vértice já existe na heap
	  if(achou > -1)
	  {
	    diminui_prioridade(n, Q,indiceViz+1, limitante[indiceViz]); //atualiza o caminho do vértice na heap (muda prioridade)
	  }else
	  {
	    //adiciona o vértice na heap caso ele não esteja lá
	    insere_lista(&n, Q, indiceViz+1, limitante[indiceViz]);
	  }  
	 }
      }else
      {
	pesoViz = G[u].lista[i].peso; //guarda o peso do vizinho atual 
	//verifica se o caminho encontrado é melhor que o caminho que já existia antes
	if(distancia[indiceViz] > distancia[u]+pesoViz) 
	{
	  anterior[indiceViz] = u+1; //marca o vértice u como anterior de seu vizinho 
	  distancia[indiceViz] = distancia[u]+pesoViz; //atualiza a distancia
	  limitante[indiceViz] = lim + distancia[indiceViz]; //atualiza o limitante do vizinho
	  achou = procuraHeap(indiceViz+1, n, Q); //verifica se o vértice já existe na heap
	  if(achou > -1)
	  {
	   //atualiza a distancia se ele já estiver na heap
	    diminui_prioridade(n, Q, indiceViz+1, limitante[indiceViz]); //atualiza o caminho do vértice na heap (muda prioridade)
	  }else
	  {
	    //adiciona o vértice na heap caso ele não esteja lá
	    insere_lista(&n, Q, indiceViz+1, limitante[indiceViz]);
	  }  
	 }
	}
      }
    a++;
   } 
  //verifica se o vértice de destino foi visitado
  if(visitado[final-1])
  {
      //imprime o peso total do caminho
      printf("1 %.3lf\n", distancia[final-1]);
      //imprime o caminho que foi percorrido
      imprimeCaminho(inicio, (final-1), anterior, distancia); //imprime o menor caminho da origem até o destino
  }
  else
  {
    printf("0 0.000\n%d 0.000 %d 0.000 ", inicio+1, final);
  }
   printf("\n%d\n", a);
   
  


}

/* Recebe as coordenadas em graus de dois vértices e 
calcula a distancia entre os dois utilizando o raio menor da terra*/
double calculaDist(vertice a, vertice b)
{
 double x, y, lat1, lat2, long1, long2, dist;
 //faz a conversão de graus para radiano
 lat1 = a.lat * PI / 180.0;
 lat2 = b.lat * PI / 180.0;
 long1 = a.longi * PI / 180.0;
 long2 = b.longi * PI / 180.0;
 
 //calcula a distancia entre os dois pontos usando o raio da terra
 x = (long2 - long1)*cos((lat1+lat2)/2);
 y = lat2 - lat1;
 dist = 6356.75*sqrt((x*x) + (y*y));
 dist = (int)(dist*1000.0)/1000.0;
 return dist;
 
}


/* Imprime o caminho (vértice e distancia) percorrido entre dois 
vértices usando o vetor de predecessores*/
void imprimeCaminho(int orig, int dest, int *prev, double *dist)
{
  //critério de parada
  if(orig != dest && prev[dest] > -1)
   imprimeCaminho(orig, (prev[dest]-1), prev, dist);
   printf("%d %.3lf ", dest+1, dist[dest]); 
}

/* Recebe duas structs e troca seus valores*/
void troca(vizinho *a, vizinho *b,int n)
{
  vizinho aux;
  aux.peso = a->peso;
  aux.num = a->num;
  a->peso = b->peso;
  a->num = b->num;
  b->num = aux.num;
  b->peso = aux.peso;
}


/* Recebe um número inteiro n > 0, uma lista de min-prioridades S e uma prio-
   ridade p e devolve a lista de min-prioridades com a nova prioridade */
void insere_lista(int *n, vizinho *S, int num, double peso)
{
  S[*n].num = num;
  S[*n].peso = peso;
  *n = *n + 1;
  sobe(*n, S, *n - 1);
}




/* Recebe um índice i em um min-heap e devolve o "pai" de i */
int pai(int i)
{
  if (i == 0)
    return 0;
  else
    return (i - 1) / 2;
}

/* Recebe um índice i em um min-heap e 
devolve o "filho esquerdo" de i */
int esquerdo(int i)
{
  return 2 * (i + 1) - 1;
}

/* Recebe um índice i em um min-heap e 
devolve o "filho direito" de i */
int direito(int i)
{
  return 2 * (i + 1);
}

/* Recebe um número inteiro n > 0, um vetor S de números in-
   teiros com n elementos e um índice i e estabelece a pro-
   priedade min-heap para a sub-árvore de S com raiz S[i] */
void desce(int n, vizinho *S, int i)
{

  int e, d, menor;
  e = esquerdo(i);
  d = direito(i);
  if (e < n && S[e].peso < S[i].peso)
    menor = e;
  else
    menor = i;
  if (d < n && S[d].peso < S[menor].peso)
    menor = d;
  if (menor != i) {
    troca(&(S[i]), &(S[menor]), n);
    desce(n, S, menor);
  }
}

/* Recebe um número inteiro n > 0, um vetor S de nú-
   meros inteiros com n elementos e um índice i e es-
   tabelece a propriedade min-heap para a árvore S */
void sobe(int n, vizinho *S, int i)
{

  while (S[pai(i)].peso > S[i].peso) {
    troca(&(S[i]), &(S[pai(i)]), n);
    i = pai(i);
  }
}

/* Recebe um número inteiro n > 0 e um vetor de números
   inteiros S com n elementos e rearranja o vetor S de
   modo que o novo vetor S possua a propriedade min-heap */
void constroi_min_heap(int n, vizinho *S)
{
  int i;
  for (i = n/2 - 1; i >= 0; i--)
    desce(n, S, i);
}

/* Recebe uma lista de min-prioridades S e devolve a menor
   prioridade em S */
double consulta_minimo(vizinho *S)
{
  return S[0].peso;
}

/* Recebe um número inteiro n > 0 e uma lista de min-priorida-
   des S e remove e devolve o valor da menor prioridade em S */
int extrai_minimo(int *n, vizinho *S, double *menor)
{
  int num;
  
  if (*n > 0) {
    
    *menor = S[0].peso;
    num = S[0].num;
    S[0].peso = S[*n - 1].peso;
    S[0].num = S[*n - 1].num;
    *n = *n - 1;
    num = num-1;
    desce(*n, S, 0);
    return num;
  }
  else
    return INT_MIN;
}

/* Recebe um número inteiro n > 0, uma lista de min-priorida-
   des S, um índice i e uma prioridade p e devolve a lista de
   min-prioridades com a prioridade na posição i modificada */
void diminui_prioridade(int n, vizinho *S, int i, double p)
{

  i = procuraHeap(i, n, S);
  if(i > -1)
  {
    if (p > S[i].peso)
    {
      printf("ERRO: nova prioridade é maior que da célula\n");    
    }
    else {
      S[i].peso = p;
      
      if(i>0)
	sobe(n, S, i);
    }
    
  }else
    printf("Elemento nao existe na heap\n\n");
}

/*Recebe um numero referente a um vértice do grafo 
e um vetor de min-heap e verifica se o vértice pertence ao vetor*/
int procuraHeap(int num, int n, vizinho *S)
{
  int i;
  
  for(i = 0; i < n; i++)
  {
   if(S[i].num == num)
   {
    return i;
   }
  }
  return -1;
  
}